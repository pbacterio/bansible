package module_unarchive

import (
	"archive/tar"
	"archive/zip"
	"compress/bzip2"
	"compress/gzip"
	"compress/lzw"
	"errors"
	"fmt"
	"io"
	"os"
	"path"
	"strings"

	"gitlab.com/pbacterio/bansible/pkg/common"
	"gitlab.com/pbacterio/bansible/pkg/module"
	"gitlab.com/pbacterio/bansible/pkg/yaml"
)

func Source(action yaml.Action) (module.Module, error) {
	m := module.Module{Imports: []string{"gitlab.com/pbacterio/bansible/pkg/modules/module_unarchive"}}

	src, exists := action.Params["src"]
	if !exists {
		return m, errors.New("required parameter \"src\" missing")
	}
	dest, exists := action.Params["dest"]
	if !exists {
		return m, errors.New("required parameter \"dest\" missing")
	}

	m.Call = fmt.Sprintf("module_unarchive.Unarchive(vars, %#v, %#v)", src, dest)
	return m, nil
}

func Unarchive(vars map[string]string, src string, dest string) error {
	src = common.ExpandParam(vars, src)
	dest = common.ExpandParam(vars, dest)
	fmt.Println("Extracting", src, "into", dest)

	archiver, compresor, err := guessArchiveType(src)
	if err != nil {
		return err
	}

	fReader, err := os.Open(src)
	if err != nil {
		return err
	}
	defer fReader.Close()

	var compressorReader io.Reader
	switch compresor {
	case "gzip":
		compressorReader, err = gzip.NewReader(fReader)
	case "bzip2":
		compressorReader, err = bzip2.NewReader(fReader), nil
	case "lzw":
		compressorReader, err = lzw.NewReader(fReader, lzw.LSB, 8), nil
	case "":
		compressorReader, err = fReader, nil
	}
	if err != nil {
		return err
	}

	switch archiver {
	case "tar":
		return unTar(compressorReader, dest)
	case "zip":
		return unZip(fReader, dest)
	}
	return nil
}

func unTar(r io.Reader, dest string) error {
	tarReader := tar.NewReader(r)
	for {
		hdr, err := tarReader.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		switch hdr.Typeflag {
		case tar.TypeDir:
			err = os.Mkdir(finalDest(dest, hdr.Name), hdr.FileInfo().Mode())
		case tar.TypeReg, tar.TypeRegA:
			dstFile, e := os.OpenFile(finalDest(dest, hdr.Name), os.O_RDWR|os.O_CREATE|os.O_TRUNC, hdr.FileInfo().Mode())
			if e != nil {
				return e
			}
			_, err = io.Copy(dstFile, tarReader)
		case tar.TypeLink:
			err = os.Link(finalDest(dest, hdr.Linkname), finalDest(dest, hdr.Name))
		case tar.TypeSymlink:
			err = os.Symlink(hdr.Linkname, finalDest(dest, hdr.Name))
		}
		if err != nil {
			return err
		}
	}
	return nil
}

func unZip(f *os.File, dest string) error {
	stat, _ := f.Stat()
	zipReader, err := zip.NewReader(f, stat.Size())
	if err != nil {
		return err
	}

	for _, f := range zipReader.File {
		rc, err := f.Open()
		if err != nil {
			return err
		}
		switch mode := f.Mode(); {
		case mode.IsDir():
			err = os.Mkdir(finalDest(dest, f.Name), f.FileInfo().Mode())
		case mode.IsRegular():
			dstFile, e := os.OpenFile(finalDest(dest, f.Name), os.O_RDWR|os.O_CREATE|os.O_TRUNC, f.FileInfo().Mode())
			if e != nil {
				return e
			}
			_, err = io.Copy(dstFile, rc)
		}
		if err != nil {
			return err
		}
	}
	return nil
}

func finalDest(baseDir string, tarPath string) string {
	if strings.HasPrefix("/", tarPath) {
		return tarPath
	}
	return path.Join(baseDir, tarPath)
}

func guessArchiveType(path string) (archiver string, compressor string, err error) {
	path = strings.ToLower(path)
	if strings.HasSuffix(path, ".tgz") || strings.HasSuffix(path, ".tar.gz") {
		return "tar", "gzip", nil
	}
	if strings.HasSuffix(path, ".tbz2") || strings.HasSuffix(path, ".tar.bz2") {
		return "tar", "bzip2", nil
	}
	if strings.HasSuffix(path, ".taz") || strings.HasSuffix(path, ".tar.Z") {
		return "tar", "lzw", nil
	}
	if strings.HasSuffix(path, ".zip") {
		return "zip", "", nil
	}
	return "", "", errors.New("unrecognized archive type")
}
