// TODO: move hash verification to a module
package module_file

import (
	"encoding/hex"
	"errors"
	"fmt"
	"hash"
	"io"
	"io/fs"
	"net/http"
	"os"
	"strconv"
	"strings"

	"gitlab.com/pbacterio/bansible/pkg/common"
	"gitlab.com/pbacterio/bansible/pkg/module"
	"gitlab.com/pbacterio/bansible/pkg/yaml"
)

var supportedHashes = map[string]bool{
	"SHA512": true,
	"SHA256": true,
	"SHA1":   true,
	"MD5":    true,
}

func Source(action yaml.Action) (module.Module, error) {
	m := module.Module{Imports: []string{"gitlab.com/pbacterio/bansible/pkg/modules/module_file"}}

	path, exists := action.Params["path"]
	if !exists {
		return m, errors.New("required parameter \"path\" missing")
	}

	src, srcExists := action.Params["src"]
	remoteSrc, remoteSrcExists := action.Params["remote-src"]
	if !srcExists && !remoteSrcExists {
		return m, errors.New("required parameter \"src\" or \"remote-src\" missing")
	}
	if !srcExists && !remoteSrcExists {
		return m, errors.New("only one parameter \"src\" or \"remote-src\" can be used at the same time")
	}

	mode := action.Params["mode"]
	if mode == "" {
		mode = "666"
	}
	if len(mode) != 3 {
		return m, errors.New("\"mode\" length has to be 3")
	}
	_, err := strconv.ParseInt(mode, 8, 32)
	if err != nil {
		return m, errors.New("\"mode\" invalid format")
	}
	mode = "0" + mode

	checksum := action.Params["checksum"]
	checksumAlg := "nil"
	checksumValue := ""
	if checksum != "" {
		checksumParts := strings.Split(checksum, ":")
		if len(checksumParts) != 2 {
			return m, errors.New("incorrect \"checksum\" format")
		}
		checksumAlg = strings.ToUpper(checksumParts[0])
		if _, ok := supportedHashes[checksumAlg]; !ok {
			return m, errors.New("hecksum algorithm not supported")
		}
		checksumAlg = fmt.Sprintf("crypto.%s.New()", checksumAlg)
		m.Imports = append(m.Imports, "crypto")
		checksumValue = checksumParts[1]
	}

	checksize := int64(0)
	checksizeParam := action.Params["checksize"]
	if checksizeParam != "" {
		checksize, err = strconv.ParseInt(action.Params["checksize"], 10, 64)
		if err != nil {
			return m, errors.New("\"checksize\" is not number")
		}
	}

	if remoteSrcExists {
		m.Call = fmt.Sprintf("module_file.Download(vars, %#v, %#v, %s, %s, %#v, %d)", remoteSrc, path, mode, checksumAlg, checksumValue, checksize)
	} else {
		m.Call = fmt.Sprintf("module_file.Copy(vars, %#v, %#v, %s, %s, %#v)", src, path, mode, checksumAlg, checksumValue)
	}
	return m, nil
}

func Copy(vars map[string]string, src string, dest string, mode int, hasher hash.Hash, checksum string) error {
	src = common.ExpandParam(vars, src)
	dest = common.ExpandParam(vars, dest)
	fmt.Println("Coping", src, "into", dest)

	srcFile, err := os.Open(src)
	if err != nil {
		return err
	}
	defer srcFile.Close()

	srcStat, err := srcFile.Stat()
	if err != nil {
		return err
	}

	destFile, err := os.OpenFile(dest, os.O_RDWR|os.O_CREATE|os.O_TRUNC, srcStat.Mode().Perm())
	if err != nil {
		return err
	}
	defer destFile.Close()

	_, err = io.Copy(destFile, srcFile)
	if err != nil {
		os.Remove(dest)
		return err
	}

	return nil
}

func Download(vars map[string]string, url string, dest string, mode int, hasher hash.Hash, checksum string, checksize uint64) error {
	url = common.ExpandParam(vars, url)
	dest = common.ExpandParam(vars, dest)
	fmt.Println("Downloading", url, "into", dest)

	if checksize > 0 || checksum != "" {
		if checkFile(dest, hasher, checksum, int64(checksize)) {
			return nil
		}
	}

	f, err := os.OpenFile(dest, os.O_RDWR|os.O_CREATE|os.O_TRUNC, fs.FileMode(mode))
	if err != nil {
		return err
	}
	defer f.Close()

	resp, err := http.Get(url)
	if err != nil {
		os.Remove(dest)
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		os.Remove(dest)
		return errors.New(resp.Status)
	}

	var reader io.Reader
	if hasher != nil {
		reader = SumReader{resp.Body, hasher}
	} else {
		reader = resp.Body
	}

	_, err = io.Copy(f, reader)
	if err != nil {
		os.Remove(dest)
		return err
	}

	if hasher != nil {
		downloadedChecksum := hex.EncodeToString(hasher.Sum(nil))
		if checksum != downloadedChecksum {
			return errors.New("Invalid checksum " + downloadedChecksum)
		}
	}
	return nil
}

type SumReader struct {
	reader io.Reader
	hasher hash.Hash
}

func (sr SumReader) Read(p []byte) (n int, err error) {
	n, err = sr.reader.Read(p)
	if n > 0 {
		sr.hasher.Write(p[:n])
	}
	return n, err
}

func checkFile(path string, hasher hash.Hash, checksum string, checksize int64) bool {
	// Check file exits
	fi, err := os.Stat(path)
	if err != nil {
		return false
	}

	// Check file size
	if checksize > 0 && checksize != fi.Size() {
		return false
	}

	// Check checksum
	if checksum == "" {
		return true
	}
	f, err := os.Open(path)
	if err != nil {
		return false
	}
	defer f.Close()
	hasher.Reset()
	defer hasher.Reset()
	_, err = io.Copy(hasher, f)
	if err != nil {
		return false
	}
	return checksum == hex.EncodeToString(hasher.Sum(nil))
}
