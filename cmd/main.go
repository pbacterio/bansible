package main

import (
	"flag"
	"fmt"
	"os"

	bansible "gitlab.com/pbacterio/bansible/internal"
)

func main() {

	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage: %s <actions.yml> \n", os.Args[0])
		flag.PrintDefaults()
	}
	flag.Parse()
	if len(flag.Args()) == 0 {
		flag.Usage()
		os.Exit(2)
	}

	err := bansible.Compile(flag.Arg(0))
	if err != nil {
		fmt.Fprintln(flag.CommandLine.Output(), err)
		os.Exit(1)
	}

}
