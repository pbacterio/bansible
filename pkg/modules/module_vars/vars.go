package module_vars

import (
	"fmt"

	"gitlab.com/pbacterio/bansible/pkg/module"
	"gitlab.com/pbacterio/bansible/pkg/yaml"
)

func Source(action yaml.Action) (module.Module, error) {
	m := module.Module{
		Imports: []string{"gitlab.com/pbacterio/bansible/pkg/modules/module_vars"},
		Call:    fmt.Sprintf("module_vars.Set(vars, %#v)", action.Params),
	}
	return m, nil
}

func Set(globalVars map[string]string, newVars map[string]string) error {
	for k, v := range newVars {
		globalVars[k] = v
	}
	return nil
}
