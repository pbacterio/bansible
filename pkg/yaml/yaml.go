package yaml

import (
	"fmt"
	"io"
	"os"

	goyaml "gopkg.in/yaml.v2"
)

type Action struct {
	Module string
	Params map[string]string
}

type RawAction map[string]map[string]string

func Parse(actionsFile string) ([]Action, error) {
	actions := []Action{}
	f, err := os.Open(actionsFile)
	if err != nil {
		return actions, err
	}
	defer f.Close()
	data, err := io.ReadAll(f)
	if err != nil {
		return actions, err
	}
	actionsMaps := []RawAction{}
	err = goyaml.Unmarshal(data, &actionsMaps)
	if err != nil {
		return actions, err
	}
	for _, actionMap := range actionsMaps {
		action, err := newAction(actionMap)
		if err != nil {
			return actions, err
		}
		actions = append(actions, *action)
	}
	return actions, nil
}

func newAction(rawAction RawAction) (*Action, error) {
	action := Action{}
	if len(rawAction) != 1 {
		return nil, fmt.Errorf("wrong action format: %s", rawAction)
	}
	for k, v := range rawAction {
		action.Module = k
		action.Params = v
	}

	return &action, nil
}
