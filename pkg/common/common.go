package common

import (
	"bytes"
	"html/template"
)

func ExpandParams(vars map[string]string, params map[string]string) {
	// data := map[string]map[string]string{"var": vars}
	for varName, varValue := range vars {
		var newValue bytes.Buffer
		_ = template.Must(template.New("").Parse(varValue)).Execute(&newValue, vars)
		vars[varName] = newValue.String()
	}
}

func ExpandParam(vars map[string]string, param string) string {
	var newValue bytes.Buffer
	_ = template.Must(template.New("").Parse(param)).Execute(&newValue, vars)
	return newValue.String()
}
