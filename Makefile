name = bansible

.PHONY: bin
bin:
	go build -o bin/${name} -ldflags="-s -w" ./cmd

.PHONY: bin
bin-multiarch:
	GOOS=linux GOARCH=amd64 go build -o bin/linux_amd64/${name} -ldflags="-s -w" ./cmd/
	GOOS=linux GOARCH=arm go build -o bin/linux_arm/${name} -ldflags="-s -w" ./cmd/
	GOOS=linux GOARCH=arm64 go build -o bin/linux_arm64/${name} -ldflags="-s -w" ./cmd/
	GOOS=darwin GOARCH=amd64 go build -o bin/darwin_amd64/${name} -ldflags="-s -w" ./cmd/

.PHONY: test
test:
	go test ./...
