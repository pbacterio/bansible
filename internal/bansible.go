package internal

import (
	_ "embed"
	"fmt"
	"os"
	"os/exec"
	"path"
	"text/template"

	"gitlab.com/pbacterio/bansible/pkg/module"
	"gitlab.com/pbacterio/bansible/pkg/modules/module_file"
	"gitlab.com/pbacterio/bansible/pkg/modules/module_unarchive"
	"gitlab.com/pbacterio/bansible/pkg/modules/module_vars"
	"gitlab.com/pbacterio/bansible/pkg/yaml"
	"golang.org/x/exp/maps"
)

//go:embed "main.tmpl"
var mainTemplate string

var modulesSource = map[string]func(action yaml.Action) (module.Module, error){
	"vars":      module_vars.Source,
	"file":      module_file.Source,
	"unarchive": module_unarchive.Source,
}

func Compile(file string) error {
	// parse yaml script
	imports, calls, err := parse(file)
	if err != nil {
		return err
	}

	// write go code
	outFile, err := os.Create(goFilename(file))
	if err != nil {
		return err
	}
	defer outFile.Close()
	t := template.Must(template.New("main").Parse(mainTemplate))
	t.Execute(outFile, map[string][]string{
		"imports": imports,
		"calls":   calls,
	})

	cmd := exec.Command("go", "build", "-o", binFilename(file), goFilename(file))
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		return err
	}
	return nil
}

func parse(file string) (calls []string, imports []string, err error) {
	actions, err := yaml.Parse(file)
	if err != nil {
		return nil, nil, err
	}
	calls = []string{}
	importsMap := map[string]bool{} // to keep imports uniq
	for _, actionFile := range actions {
		sourceFunc, ok := modulesSource[actionFile.Module]
		if !ok {
			return nil, nil, fmt.Errorf("module: \"%s\" does not exist", actionFile.Module)
		}
		action, err := sourceFunc(actionFile)
		if err != nil {
			return nil, nil, err
		}

		calls = append(calls, action.Call)
		for _, toImport := range action.Imports {
			importsMap[toImport] = true
		}
	}
	return maps.Keys(importsMap), calls, nil
}

func goFilename(ymlFilename string) string {
	dir, file := path.Split(ymlFilename)
	ext := path.Ext(file)
	if ext != "" {
		file = file[:len(file)-len(ext)]
	}
	file += ".go"
	return path.Join(dir, file)
}

func binFilename(ymlFilename string) string {
	dir, file := path.Split(ymlFilename)
	ext := path.Ext(file)
	if ext != "" {
		file = file[:len(file)-len(ext)]
	} else {
		file += ".bin"
	}
	return path.Join(dir, file)
}
